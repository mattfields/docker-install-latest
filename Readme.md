# Docker community edition installer script

Install script for Docker Community edition, for Ubuntu, based on Docker community edition instructions at:
https://store.docker.com/editions/community/doc


## Description
This is just a helper script for installing the latest version of Docker on Ubuntu.

Currently, the Docker version in the Ubuntu repositories lags behind by several updates. This will install the latest release.

## Notes
This will work on Ubuntu versions 16.10, 16.04, or 14.04, 64-bit only.

(Docker is supported on 64-bit distributions only)


## Usage

1. Download this repository
2. `chmod +x docker-install-latest.sh`
3. `./docker-install-latest.sh`


## Maintainer
 - **Maintainer:** Matt Fields <mattfields.china@gmail.com>
 - **Disclaimer**: I have no relation to Docker nor Canonical. Use at your own risk.


