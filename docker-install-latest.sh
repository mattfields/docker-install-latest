# Docker-install-latest.sh
# Installs the latest version of docker on Ubuntu 16.04-16.10



# add necessary components

echo "This will install the latest version of Docker community edition on your machine."
echo "You must have 64-bit version Ubuntu 14.04, 16.04, or 16.10"

echo -e "\nInstallation steps:"
echo "1. Install pre-installation components"
echo "2. Install docker gpg key"
echo "3. Add docker repository "
echo "4. Install docker"
echo "5. Test docker with 'hello world' script"

echo -e "\nPress any key to continue, Ctrl-C to quit"
read -s -n1 NULL

echo -e "\n########################################################"
echo -e "Installing pre-installation components\n"
sudo apt-get -y install \
  apt-transport-https \
  ca-certificates \
  curl


# Add docker repository
echo -e "\n########################################################\n"
echo -e "Installing docker gpg key\n"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo -e "\n########################################################\n"
echo -e "Adding docker repository\n"
sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"

sudo apt-get update



# Install docker
echo -e "\n########################################################\n"
echo -e "Installing docker\n"

sudo apt-get -y install docker-ce


# Test docker
echo -e "\n########################################################\n"
echo -e "Testing docker \"hello world\"\n"
sudo docker run hello-world
